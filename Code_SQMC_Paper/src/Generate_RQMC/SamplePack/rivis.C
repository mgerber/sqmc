/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// SAMPLE PACKage                                                          //
//                                                                         //
// Example: visualization of radical inverse point sets                    //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// Copyright (C) 2002 by Thomas Kollig (kollig@informatik.uni-kl.de)       //
//                       Alexander Keller (keller@informatik.uni-kl.de)    //
//                                                                         //
// All rights reserved. You may not distribute this software, in whole or  //
// in part, especially not as part of any commercial product, without the  //
// express consent of the authors.                                         //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdio.h>
#include <fstream.h>

#include "RadicalInversesBase2.h"

uint rng32()
{
  return (uint)(drand48()*((double)((uint)-1)+1.0));
}

typedef enum _nt
{
  nt_Sobol,
  nt_Hammersley,
  nt_LarcherPillichshammer
} nt;

class Options
{
public:
  
  nt   ntype;         // type of net
  bool rds;           // random digit scrambling
  int  n;             // number of points in net
  char filename[255]; // output filename

  Options(int argc, char **argv)
    {
      ntype = nt_Sobol;
      rds = false;
      n = 16;
      sprintf(filename, "ri.tex");
      for (int i = 1; i < argc; ++i)
	if (!strcmp("-nt", argv[i]))
	  if (!strcmp("Sobol", argv[++i]))
	    ntype = nt_Sobol;
	  else if (!strcmp("Hammersley", argv[i]))
	    ntype = nt_Hammersley;
	  else if (!strcmp("LarcherPillichshammer", argv[i]))
	    ntype = nt_LarcherPillichshammer;
	  else
	    {
	      cerr << "Unknown generator type: " << argv[i] << endl;
	      cerr << "Possible types: Sobol, Hammersley, LarcherPillichshammer" << endl;
	      exit(-1);
	    }
	else if (!strcmp("-rds", argv[i]))
	  rds = true;
	else if (!strcmp("-n", argv[i]))
	  {
	    n = atoi(argv[++i]);
	    if (n < 1)
	      {
		cerr << "The net should have at least one point" << endl;
		exit(-1);
	      }
	    if (n > 16384)
	      {
		cerr << "Nets with maximal 16384 points can be printed due to LaTeX" << endl;
		exit(-1);
	      }
	  }
	else if (!strcmp("-o", argv[i]))
	  sprintf(filename, "%s.tex", argv[++i]);
	else if (!strcmp("-help", argv[i]))
	  {
	    cout << "Usage: " << argv[0] << " [-nt nettype] [-rds] [-n points] [-o filename]" << endl;
	    exit(0);
	  }
	else
	  {
	    cerr << "Unknown option: " << argv[i] << endl;
	    cerr << "Usage: " << argv[0] << " [-nt nettype] [-rds] [-n points] [-o filename]" << endl;
	    exit(-1);
	  }
    }
};


int main(int argc, char **argv)
{
  int    i;
  uint   rnd1, rnd2;

  Options  o(argc, argv);
  ofstream out(o.filename);
  
  if (o.rds)
    {
      rnd1 = rng32();
      rnd2 = rng32();
    }
  else
    rnd1 = rnd2 = 0;
  
  out << "\\documentclass[a4paper,landscape]{slides}\n"
      << "\\newif\\ifpdf\n"
      << "\\ifx\\pdfoutput\\undefined\n"
      << "  \\pdffalse\n"
      << "\\else\n"
      << "  \\pdfoutput=1\n"
      << "  \\pdfcompresslevel=9\n"
      << "  \\pdftrue\n"
      << "\\fi\n"
      << "\\ifpdf\n"
      << "\\usepackage{hyperref}\n"
      << "\\hypersetup{\n"
      << "  pdftitle={(t,m,s)-net},\n"
      << "  pdfauthor={DiscPack Version 0.1},\n"
      << "  pdfpagemode={FullScreen}\n"
      << "}\n"
      << "\\fi\n"
      << "\\begin{document}\n"
      << "\\pagestyle{empty}\n"
      << "\\begin{slide}\n"
      << "\\begin{center}\n"
      << "\\setlength{\\unitlength}{0.012cm}\n"
      << "\\begin{picture}(1000,1000)\n"
      << "\\thicklines\n"
      << "\\multiput(0,0)(1000,0){2}{\\line(0,1){1000}}\n"
      << "\\multiput(0,0)(0,1000){2}{\\line(1,0){1000}}\n";
  switch (o.ntype)
    {
    case nt_Sobol:
      for (i = 0; i < o.n; ++i)
	out << "\\put(" << (int)(RI_vdC(i, rnd1)*1000 + 0.5) << ","
	    << (int)(RI_S(i, rnd2)*1000 + 0.5) << "){\\circle*{20}}\n";
      break;
    case nt_Hammersley:
      for (i = 0; i < o.n; ++i)
	out << "\\put(" << (int)(RI_Bn(i, o.n, rnd1)*1000 + 0.5) << ","
	    << (int)(RI_vdC(i, rnd2)*1000 + 0.5) << "){\\circle*{20}}\n";
      break;
    case nt_LarcherPillichshammer:
      for (i = 0; i < o.n; ++i)
	out << "\\put(" << (int)(RI_Bn(i, o.n, rnd1)*1000 + 0.5) << ","
	    << (int)(RI_LP(i, rnd2)*1000 + 0.5) << "){\\circle*{20}}\n";
      break;
    }
  out << "\\end{picture}\n"
      << "\\end{center}\n"
      << "\\end{slide}\n"
      << "\\end{document}" << endl;
  out.close();
  
  return 0;
}

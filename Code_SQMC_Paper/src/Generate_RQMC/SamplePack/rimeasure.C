/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// SAMPLE PACKage                                                          //
//                                                                         //
// Example: measurements of radical inverses point sets                    //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// Copyright (C) 2002 by Thomas Kollig (kollig@informatik.uni-kl.de)       //
//                       Alexander Keller (keller@informatik.uni-kl.de)    //
//                                                                         //
// All rights reserved. You may not distribute this software, in whole or  //
// in part, especially not as part of any commercial product, without the  //
// express consent of the authors.                                         //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <iostream.h>

#include "RadicalInversesBase2.h"
#include "Measurements.h"

uint rng32()
{
  return (uint)(drand48()*((double)((uint)-1)+1.0));
}

typedef enum _measure
{
  measure_L2,
  measure_Star,
  measure_MinDist,
  measure_MinDistTorus
} measure;

typedef enum _nt
{
  nt_Sobol,
  nt_Hammersley,
  nt_LarcherPillichshammer
} nt;

class Options
{
public:
  
  measure dtype;         // type of discrepancy
  nt      ntype;         // type of net
  bool    ds;            // digit scrambling
  int     n;             // number of points in net
  bool    sequence;      // calculate discrepancy up to n
  
  Options(int argc, char **argv)
    {
      dtype = measure_L2;
      ntype = nt_Sobol;
      ds = false;
      n = 16;
      sequence = false;
      for (int i = 1; i < argc; ++i)
	if (!strcmp("-measure", argv[i]))
	  if (!strcmp("L2", argv[++i]))
	    dtype = measure_L2;
	  else if (!strcmp("Star", argv[i]))
	    dtype = measure_Star;
	  else if (!strcmp("MinDist", argv[i]))
	    dtype = measure_MinDist;
	  else if (!strcmp("MinDistTorus", argv[i]))
	    dtype = measure_MinDistTorus;
	  else
	    {
	      cerr << "Unknown measurement type: " << argv[i] << endl;
	      cerr << "Possible types: L2, Star, MinDist, MinDistTorus" << endl;
	      exit(-1);
	    }
	else if (!strcmp("-nt", argv[i]))
	  if (!strcmp("Sobol", argv[++i]))
	    ntype = nt_Sobol;
	  else if (!strcmp("Hammersley", argv[i]))
	    ntype = nt_Hammersley;
	  else if (!strcmp("LarcherPillichshammer", argv[i]))
	    ntype = nt_LarcherPillichshammer;
	  else
	    {
	      cerr << "Unknown generator type: " << argv[i] << endl;
	      cerr << "Possible types: Sobol, Hammersley, LarcherPillichshammer" << endl;
	      exit(-1);
	    }
	else if (!strcmp("-ds", argv[i]))
	  ds = true;
	else if (!strcmp("-n", argv[i]))
	  {
	    n = atoi(argv[++i]);
	    if (n < 1)
	      {
		cerr << "The net should have at least one point" << endl;
		exit(-1);
	      }
	  }
	else if (!strcmp("-s", argv[i]))
	  sequence = true;
	else if (!strcmp("-help", argv[i]))
	  {
	    cout << "Usage: " << argv[0] << " [-measure measurementtype] [-nt nettype] [-ds] [-n points] [-s]" << endl;
	    exit(0);
	  }
	else
	  {
	    cerr << "Unknown option: " << argv[i] << endl;
	    cerr << "Usage: " << argv[0] << " [-measure measurementtype] [-nt nettype] [-ds] [-n points] [-s]" << endl;
	    exit(-1);
	  }
    }
};


int main(int argc, char **argv)
{
  int    i, j;
  uint   rnd1, rnd2;
  double *p;

  Options o(argc, argv);
  
  p = AllocPointMemory(2, o.n);
  if (o.ds)
    {
      rnd1 = rng32();
      rnd2 = rng32();
    }
  else
    rnd1 = rnd2 = 0;
  
  if (o.sequence)
    {
      switch (o.ntype)
	{
	case nt_Sobol:
	  for (i = 0; i < o.n; ++i)
	    {
	      p[i*2] = RI_vdC(i, rnd1);
	      p[i*2+1] = RI_S(i, rnd2);
	    }
	  switch (o.dtype)
	    {
	    case measure_L2:
	      for (i = 1; i <= o.n; ++i)
		cout << i << " " << L2Discrepancy(2, i, p) << endl;
	      break;
	    case measure_Star:
	      for (i = 1; i <= o.n; ++i)
		cout << i << " " << StarDiscrepancy(2, i, p) << endl;
	      break;
	    case measure_MinDist:
	      for (i = 1; i <= o.n; ++i)
		cout << i << " " << MinimumDistance(2, i, p) << endl;
	      break;
	    case measure_MinDistTorus:
	      for (i = 1; i <= o.n; ++i)
		cout << i << " " << MinimumDistanceTorus(2, i, p) << endl;
	      break;
	    }
	  break;
	case nt_Hammersley:
	  for (i = 0; i < o.n; ++i)
	    p[i*2] = RI_vdC(i, rnd2);
	  for (j = 1; j <= o.n; ++j)
	    {
	      for (i = 0; i < j; ++i)
		p[i*2+1] = RI_Bn(i, j, rnd1);
	      switch (o.dtype)
		{
		case measure_L2:
		  cout << j << ": " << L2Discrepancy(2, j, p) << endl;
		  break;
		case measure_Star:
		  cout << j << ": " << StarDiscrepancy(2, j, p) << endl;
		  break;
		case measure_MinDist:
		  cout << j << ": " << MinimumDistance(2, j, p) << endl;
		  break;
		case measure_MinDistTorus:
		  cout << j << ": " << MinimumDistanceTorus(2, j, p) << endl;
		  break;
		}
	    }
	  break;
	case nt_LarcherPillichshammer:
	  for (i = 0; i < o.n; ++i)
	    p[i*2+1] = RI_LP(i, rnd2);
	  for (j = 1; j <= o.n; ++j)
	    {
	      for (i = 0; i < j; ++i)
		p[i*2] = RI_Bn(i, j, rnd1);
	      switch (o.dtype)
		{
		case measure_L2:
		  cout << j << ": " << L2Discrepancy(2, j, p) << endl;
		  break;
		case measure_Star:
		  cout << j << ": " << StarDiscrepancy(2, j, p) << endl;
		  break;
		case measure_MinDist:
		  cout << j << ": " << MinimumDistance(2, j, p) << endl;
		  break;
		case measure_MinDistTorus:
		  cout << j << ": " << MinimumDistanceTorus(2, j, p) << endl;
		  break;
		}
	    }
	  break;
	}
    }
  else
    {
      switch (o.ntype)
	{
	case nt_Sobol:
	  for (i = 0; i < o.n; ++i)
	    {
	      p[i*2] = RI_vdC(i, rnd1);
	      p[i*2+1] = RI_S(i, rnd2);
	    }
	  break;
	case nt_Hammersley:
	  for (i = 0; i < o.n; ++i)
	    {
	      p[i*2] = RI_Bn(i, o.n, rnd1);
	      p[i*2+1] = RI_vdC(i, rnd2);
	    }
	  break;
	case nt_LarcherPillichshammer:
	  for (i = 0; i < o.n; ++i)
	    {
	      p[i*2] = RI_Bn(i, o.n, rnd1);
	      p[i*2+1] = RI_LP(i, rnd2);
	    }
	  break;
	}
      switch (o.dtype)
	{
	case measure_L2:
	  cout << o.n << ": " << L2Discrepancy(2, o.n, p) << endl;
	  break;
	case measure_Star:
	  cout << o.n << ": " << StarDiscrepancy(2, o.n, p) << endl;
	  break;
	case measure_MinDist:
	  cout << o.n << ": " << MinimumDistance(2, o.n, p) << endl;
	  break;
	case measure_MinDistTorus:
	  cout << o.n << ": " << MinimumDistanceTorus(2, o.n, p) << endl;
	  break;
	}
    }
  
  FreePointMemory(p, 2);
  
  return 0;
}

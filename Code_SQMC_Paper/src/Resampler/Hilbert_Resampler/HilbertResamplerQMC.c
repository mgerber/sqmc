

#include "../../../include/Resampler/Hilbert_Resampler/HilbertResamplerQMC.h"

#include "../../../include/functionsCC.hpp"

	
/*****************************************************************************************************************
			RESAMPLER ALGORITHMS FOR FILTERING
******************************************************************************************************************/

void HilbertResampler(double *quasi,  double *x, int dx, int N, double *W, double* xh)
{
	size_t *J1=(size_t*)malloc(sizeof(size_t)*N);

	if(dx==1)
	{
		gsl_heapsort_index(J1, x, (size_t)N, sizeof(double), compare_double);
	}
	else
	{
		Hilbert_Sort_index(x, dx, N, J1);
	}
	
	quasi_Resample(quasi, dx, N, W, J1, x, xh);

	free(J1);
	J1=NULL;
}


void quasi_Resample(double *quasi, int dx,  int N, double *W, size_t *J1, double *x, double *xh)
{
	int i,j,k;
	double s;
	
    	s=0;
	j=0;

	for(i=0; i<N; i++)
	{
		s+=W[(int)J1[i]];

		while(quasi[(dx+1)*j]<=s && j<N)
		{
			for(k=0;k<dx;k++)
			{
				xh[dx*j+k]=x[(int)(dx*J1[i]+k)];
			}
			j++;

		}
		if(j==N)
		{
			break;
		}
	     
	}
}


int* ForHilbertResampler(double *quasi, int dimQuasi,  double *x, int dx, int N, int Nb, double *W, double* xh)
{
	int i;
	size_t *J1=(size_t*)malloc(sizeof(size_t)*N);

	if(dx==1)
	{
		gsl_heapsort_index(J1, x, (size_t)N, sizeof(double), compare_double);
	}
	else
	{
		Hilbert_Sort_index(x, dx, N, J1);
	}
	
	quasi_ResampleFor(quasi, dimQuasi, dx, N, Nb, W, J1, x, xh);

	int *J2=(int*)malloc(sizeof(int)*N);

	for(i=0;i<N;i++)
	{
		J2[i]=J1[i];
	}
	free(J1);
	J1=NULL;

	return(J2);
		
}




void quasi_ResampleFor(double *quasi, int dimQuasi, int dx,  int N, int Nb, double *W, size_t *J1, double *x, double *xh)
{
	int i,j,k;
	double s;
	
    	s=0;
	j=0;

	for(i=0; i<N; i++)
	{
		s+=W[(int)J1[i]];

		while(quasi[dimQuasi*j]<=s && j<Nb)
		{
			for(k=0;k<dx;k++)
			{
				xh[dx*j+k]=x[(int)(dx*J1[i]+k)];
			}

			j++;
		}
		if(j==Nb)
		{
			break;
		}
	     
	}
}

/*****************************************************************************************************************
			RESAMPLER ALGORITHMS FOR  BACKWARD STEP
******************************************************************************************************************/


int* BackHilbertResampler(double *quasi, int dimQuasi, double *x, int dx, int N, int Nb, double *W, double* xh)
{
	int i;
	
	size_t *J1=(size_t*)malloc(sizeof(size_t)*N);
	int *J2=(int*)malloc(sizeof(int)*Nb);

	if(dx==1)
	{
		gsl_heapsort_index(J1, x, (size_t)N, sizeof(double), compare_double);
	}
	else
	{
		Hilbert_Sort_index(x, dx, N, J1);
	}
		
	quasi_ResampleBack(quasi, dimQuasi, dx, N, Nb, W, J1, J2, x, xh);

	free(J1);
	J1=NULL;
	return(J2);
		
}






void quasi_ResampleBack(double *quasi, int dimQuasi, int dx,  int N, int Nb, double *W, size_t *J1, int *J2, double *x, double *xh)
{
	int i,j,k;
	double s;
	
    	s=0;
	j=0;

	for(i=0; i<N; i++)
	{
		s+=W[(int)J1[i]];

		while(quasi[dimQuasi*j]<=s && j<Nb)
		{
			for(k=0;k<dx;k++)
			{
				xh[dx*j+k]=x[(int)(dx*J1[i]+k)];
			}
			J2[j]=J1[i];
			j++;
		}
		if(j==Nb)
		{
			break;
		}
	     
	}
}

	









		
	


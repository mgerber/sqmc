############################################################################################################################################
#		MULTIVARIATE STOCHASTIC VOLATILITY MODEL 
############################################################################################################################################

#SQMC_Univ: FILTERING AND LIKLIHOOD ESTIMATION

#INPUTS:

#y=(y_0,...,Y_{T-1}): dx*T vector of data
#dx		    : dimensions of of the hidden process
#theta	 	    : vector of parameters, theta=c(mu,Phi,Psi,C)
#N		    : number of particles
#qmc		    : if TRUE, Sobol' sequence is used as input
#		      if FALSE, SMC is used
#scrambling    : if TRUE, Owen's scambling is applied to the Sobol' sequence
#computeExp   : if TRUE the algorithm compute E[X_t|y_{0:t}]
#num_samples   : number of SMC/SQMC runs
#seed		    : used for SMC (if seed<0, seed is taken at random)

#OUTPUTS	

#L		    : matrix of size (T times num_samples) giving num_samples estimations of the log-likelihood at time t=0,...,T-1
#EX		    : matrix of size (T times dx) giving estimate of E[X_t|Y_{0:t}] at time t=0,...,T-1
#EX2		    : matrix of size (T times dx) giving estimate of E[X^2_t|Y_{0:t}] at time t=0,...,T-1


#IMPORTANT NOTE: the prior distribution for X_0 assumed that the latent process is stationnary

############################################################################################################################################


SQMC_SV=function(y, dx, theta, N,  qmc=TRUE, scrambling=TRUE,  computeExp=FALSE, num_samples=1, seed=-1){
	ExtraParam=0
	if(computeExp){
	  ExtraParam=1
     }
	if(qmc){
	  qmc2=1
	  if(scrambling){
		scr2=1
	  }else{
		scr2=0
	  }
	}else{
       qmc2=0
	  scr2=0
	}
	T=nrow(y)
	dy=ncol(y)

	##precompute some quantities that will be used to evaluate the potential functions

	mu=theta[1:dx]

	Phi=matrix(theta[(dx+1):(dx+dx^2)],dx,dx,byrow=F)

	Psi=matrix(theta[(dx+dx^2+1):(dx+2*dx^2)],dx,dx,byrow=F)

	C=matrix(theta[(dx+2*dx^2+1):(dx+6*dx^2)],2*dx,2*dx,byrow=F)

	C1=C[1:dx,1:dx]
	C2=C[(dx+1):(2*dx),(dx+1):(2*dx)]
	C12=C[1:dx,(dx+1):(2*dx)]

	Sx=sqrt(Psi)%*%C2%*%sqrt(Psi)

	Sy=C1-C12%*%solve(C2)%*%t(C12)
	A=C12%*%solve(C2)%*%solve(sqrt(Psi))
	APhi=A%*%Phi

	theta2=c(mu,t(APhi),t(A), solve(Sy), Sx, t(Phi),log(det(Sy)))
		
	##parameter of the prior distribution (stationnary distribution of X_t)

	vec=solve(diag(1,dx^2)-Phi%x%Phi)%*%matrix(c(Sx),dx^2)

	theta2=c(theta2,mu,vec)

	filter=.C("SQMC_SV",as.double(c(t(y))), as.integer(dy), as.integer(dx), as.integer(T), 
	as.double(theta2), as.integer(seed), as.integer(num_samples), as.integer(N),as.integer(qmc2), as.integer(scr2),
	as.integer(ExtraParam), lik=double(num_samples*T), expx=double(dx*T), expx2=double(dx*T))
		
	return(list(L=matrix(filter$lik,T,num_samples), EX=matrix(filter$expx,T,dx,byrow=T), EX2=matrix(filter$expx2,T,dx,byrow=T)))
		
}

############################################################################################################################################
	#SQMC_Back_SV: QMC FORWARD-BACKWARD SMOOTHING ALGORITHM

#INPUTS:

#y=(y_0,...,Y_{T-1}): dx*T vector of data
#dx		    : dimensions of of the hidden process
#theta	    : vector of parameters, theta=c(mu,Phi,Psi,C)
#N_forward    : number of particles used in the forward step
#N_backward   : number of particles used in the backward step
#qmc_forward  : if TRUE, Owens's scrambled Sobol' sequence is used as imput (forward step)
#		      if FALSE, SMC is used (forward step)
#qmc_backward : if TRUE, Owens's scrambled Sobol' sequence is used as imput (Backward step)
#		      if FALSE, SMC is used (Backward step)
#marginal	    : if TRUE, estimate the matrginal smoothing distributions,
# 			if FALSE, estimate full smoothing distribution
#num_samples  : number of SMC/SQMC runs
#seed	    : used for SMC (if seed<0, seed is taken at random)

#OUTPUTS	
#L		    : matrix of size (T times num_samples) giving num_samples estimations of the log-likelihood at time t=0,...,T-1
#EX		    : matrix of size (T times dx) giving estimate of E[X_t|Y_{0:T-1}] at time t=0,...,T-1
#EX2		    : matrix of size (T times dx) giving estimate of E[X^2_t|Y_{0:T-1}] at time t=0,...,T-1


#IMPORTANT NOTE: the prior distribution for X_0 assumed that the latent process is stationnary

############################################################################################################################################

SQMCBack_SV=function(y, dx, theta, N_forward, N_backward,  qmc_forward=TRUE, qmc_backward=TRUE, marginal=TRUE,  num_samples=1,seed=-1){
	if(qmc_forward){
	  qmc2=1
	}else{
       qmc2=0
	}
	if(qmc_backward){
	  qmcb2=1
	}else{
       qmcb2=0
	}
	if(marginal){
	  Marg=1
	}else{
       Marg=0
	}
	T=nrow(y)
	dy=ncol(y)

	##precompute some quantities that will be used to evaluate the potential functions

	mu=theta[1:dx]

	Phi=matrix(theta[(dx+1):(dx+dx^2)],dx,dx,byrow=F)

	Psi=matrix(theta[(dx+dx^2+1):(dx+2*dx^2)],dx,dx,byrow=F)

	C=matrix(theta[(dx+2*dx^2+1):(dx+6*dx^2)],2*dx,2*dx,byrow=F)

	C1=C[1:dx,1:dx]
	C2=C[(dx+1):(2*dx),(dx+1):(2*dx)]
	C12=C[1:dx,(dx+1):(2*dx)]

	Sx=sqrt(Psi)%*%C2%*%sqrt(Psi)

	Sy=C1-C12%*%solve(C2)%*%t(C12)
	A=C12%*%solve(C2)%*%solve(sqrt(Psi))
	APhi=A%*%Phi

	theta2=c(mu,t(APhi),t(A), solve(Sy), Sx, t(Phi),log(det(Sy)))
		
	##parameter of the prior distribution (stationnary distribution of X_t)

	vec=solve(diag(1,dx^2)-Phi%x%Phi)%*%matrix(c(Sx),dx^2)

	theta2=c(theta2,mu,vec)

	filter=.C("SQMCBack_SV",as.double(c(t(y))), as.integer(dy), as.integer(dx), as.integer(T), 
	as.double(theta2), as.integer(seed), as.integer(num_samples), as.integer(N_forward), as.integer(N_backward), 
	as.integer(qmc2),	as.integer(qmcb2),  as.integer(Marg),  lik=double(num_samples*T), expx=double(dx*T), expx2=double(dx*T))
		
	return(list(L=matrix(filter$lik,T,num_samples), EX=matrix(filter$expx,T,dx,byrow=T), EX2=matrix(filter$expx2,T,dx,byrow=T)))

}

		












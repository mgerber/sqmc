
#include <R.h>
#include "../include/SMC_Back.h"

/****************************************************************************************************
				 FORWARD-BACKWARD SMC TO TARGET SMOOTHING DISTRIBUTION
****************************************************************************************************/

void SMC_ForBack(double *y, int dy, int dx, int T, double *theta, int N, int Nb, gsl_rng *random, ParamTransitionPF paramTransition, 
ResamplingPF resampling, SimInitPF simInit, TransitionPF transition, SimTransitionPF simTransition, PotentialPF potential,int *par, 
double *lik, double *expx)
{
	int i, j, k, iter, n=N*dx;
	double cc;
	
	double *W=(double*)malloc(sizeof(double)*N);
	double *storeW=(double*)malloc(sizeof(double)*(N*T));

	double *x=(double*)malloc(sizeof(double)*n);
	double *storeX=(double*)malloc(sizeof(double)*(T*n));
	
	//FORWARD PASS

	SMC_Forward(y, dy, dx, T, theta, N,  random, paramTransition, resampling, simInit, simTransition, potential,
	par, lik, storeX, storeW, x, W);

	//BACKWARD PASS

	double *xT=(double*)malloc(sizeof(double)*(Nb*dx));

	SMC_Back(dx, T, 0, theta, N, Nb, random, resampling, transition, x, storeX, W, storeW, expx, xT);

	free(x);
	x=NULL;
	free(xT);
	xT=NULL;
	free(W);
	W=NULL;
	free(storeW);
	storeW=NULL;
	free(storeX);
	storeX=NULL;
	
}


/****************************************************************************************************
				 FORWARD-BACKWARD SMC TO TARGET SMOOTHING DISTRIBUTION
****************************************************************************************************/

void SMC_ForBackMarg(double *y, int dy, int dx, int T, double *theta, int N,  gsl_rng *random, ParamTransitionPF paramTransition, 
ResamplingPF resampling, SimInitPF simInit, TransitionPF transition, SimTransitionPF simTransition, PotentialPF potential,int *par, 
double *lik, double *expx)
{
	int i, j, k, iter, n=N*dx;
	double cc;
	
	double *W=(double*)malloc(sizeof(double)*N);
	double *storeW=(double*)malloc(sizeof(double)*(N*T));

	double *x=(double*)malloc(sizeof(double)*n);
	double *storeX=(double*)malloc(sizeof(double)*(T*n));
	
	//FORWARD PASS

	SMC_Forward(y, dy, dx, T, theta, N,  random, paramTransition, resampling, simInit, simTransition, potential,
	par, lik, storeX, storeW, x, W);

	//BACKWARD PASS


	SMC_BackMarg(dx, T, theta, N, transition, x, storeX, W, storeW, expx);

	free(x);
	x=NULL;
	free(W);
	W=NULL;
	free(storeW);
	storeW=NULL;
	free(storeX);
	storeX=NULL;
	
}

/***********************************************************************************************************************************/

/****************************************************************************************************
				BACKWARD STEP OF SMC (TARGET SMOOTHING DISTRIBUTION) 
****************************************************************************************************/


void SMC_Back(int dx, int T, int T_in, double *theta, int N, int Nb, gsl_rng *random, ResamplingPF resampling, TransitionPF transition, 
double *x, double *storeX, double *W, double *storeW, double *expx, double *xh)
{
	int i, j, k, iter, n=N*dx;
	double cc;
	
	double *xhh=NULL, *wei=NULL;
	
	iter= T-1;

	for(i=0;i<T*dx;i++)
	{
		expx[i]=0;
	}

	(*resampling)(random, x, dx, N, Nb, W, xh);   

	for(k=0;k<dx;k++)
	{
		for(j=0;j<Nb;j++)
		{
			expx[dx*iter+k]+=xh[dx*j+k];
		}
		expx[dx*iter+k]/=(double)(Nb);
		
	}

	xhh=(double*)malloc(sizeof(double)*dx);
	wei=(double*)malloc(sizeof(double)*N);

	for(iter= T-2; iter>=T_in; iter--)
	{
		for(k=0;k<dx;k++)
		{
			for(i=0; i<N; i++)
			{
				x[dx*i+k]=storeX[n*iter+dx*i+k];
			}
		}

		for(j=0; j<Nb; j++)
		{

			for(k=0;k<dx;k++)
			{
				xhh[k]=xh[dx*j+k];
			}
			
			(*transition)(xhh, x, dx, iter+1, N, theta, wei);

			for(i=0;i<N;i++)
			{
				wei[i]+=storeW[N*iter+i];
			}

			cc=weight(wei,W,N); 

			//sample a new particle

			ResampleBack(gsl_rng_uniform_pos(random), x, dx, N, W, j, xh);   

			for(k=0;k<dx;k++)
			{
				expx[dx*iter+k]+=xh[dx*j+k]/((double) Nb);
			}

		}
	}


	free(xhh);
	xhh=NULL;
	free(wei);
	wei=NULL;
}


/****************************************************************************************************
				BACKWARD STEP OF SQMC (TARGET MARGINAL SMOOTHING DISTRIBUTIONS) 
****************************************************************************************************/


void SMC_BackMarg(int dx, int T,  double *theta, int N,  TransitionPF transition, 
double *x, double *storeX, double *W, double *storeW, double *expx)
{
	int i, k, j,iter, n=N*dx;
	double cc;
	
	double *xhh=NULL, *wei=NULL, *Wb=NULL;
	
	//BACKWARD PASS
	iter= T-1;

	for(i=0;i<T*dx;i++)
	{
		expx[i]=0;
	}

	Wb=(double*)malloc(sizeof(double)*N);

	for(k=0; k<dx; k++)
	{
		for(i=0;i<N;i++)
		{
			expx[dx*iter+k]+=W[i]*storeX[n*iter+dx*i+k];
		}
	}
	
	for(i=0;i<N;i++)
	{
		Wb[i]=storeW[N*iter+i];
	}

	cc=weight(Wb,Wb,N);

	xhh=(double*)malloc(sizeof(double)*dx);
	wei=(double*)malloc(sizeof(double)*N);

	//2.3 Iterate

	for(iter= T-2; iter>=0; iter--)
	{	
		for(i=0; i<N; i++)
		{
			for(k=0;k<dx;k++)
			{
				x[dx*i+k]=storeX[n*iter+dx*i+k];
			}
			W[i]=0;
		}

		for(j=0;j<N;j++)
		{
			for(k=0;k<dx;k++)		
			{
				xhh[k]=storeX[n*(iter+1)+dx*j+k];
			}

			(*transition)(xhh, x, dx, iter+1, N, theta, wei);

			for(i=0;i<N;i++)
			{
				wei[i]+=storeW[N*iter+i];
			}

			cc=weight(wei,wei,N);

			for(i=0;i<N;i++)
			{	
				W[i]+=exp(log(Wb[j])+log(wei[i]));
			}
		}

		for(i=0;i<N;i++)
		{
			Wb[i]=W[i];
		}

		//2.3.2 compute expectaion of x_{iter} w.r.t. the smoothing distribution at time "iter"

		for(k=0; k<dx; k++)
		{
			for(i=0;i<N;i++)
			{
				expx[dx*iter+k]+=W[i]*x[dx*i+k];
			}
		}
		
		
	}
	free(xhh);
	xhh=NULL;
	free(wei);
	wei=NULL;
	free(Wb);
	Wb=NULL;
	
}






#include <R.h>
#include "../include/SQMC_Back.h"




/****************************************************************************************************
				BACKWARD STEP OF SQMC (TARGET SMOOTHING DISTRIBUTION) 
****************************************************************************************************/



void SQMC_Back(int dx, int T,  int T_in, double *theta, int N, int Nb, int qmcB, ResamplingBack_QMC resampling, TransitionPF transition, 
 double *x, double *storeX, double *W, double *storeW, double *expx, double *xh)
{
	int i, j, k, iter, n=N*dx;
	double cc;
	
	double *xhh=NULL, *sim=NULL, *wei=NULL;
	int *J=NULL;

	for(i=0; i<T*dx; i++)
	{
		expx[i]=0;
	}

	//Generate a RQMC point set in (0,1)^T (nested scrambling is used)

	sim=(double*)malloc(sizeof(double)*(Nb*T));
	
	Scrambled *os1 = Scrambled_Create(qmcB, T, Nb);

	Scrambling(os1);

	getPoints(os1, T, Nb, sim);

	Scrambled_Destroy(os1);

	os1=NULL;
	
	//Sample  particles  at time T-1

	iter=T-1;

	J=(*resampling)(sim, T, x, dx, N, Nb, W, xh);	

	for(k=0; k<dx; k++)
	{
		for(j=0;j<Nb;j++)
		{
			expx[dx*iter+k]+=xh[dx*j+k]/((double)Nb);
		}
	}


	free(J);
	J=NULL;
	
	xhh=(double*)malloc(sizeof(double)*dx);
	wei=(double*)malloc(sizeof(double)*N);
		
	//Start iterate

	for(iter= T-2; iter>=T_in; iter--)
	{
		//Set of particles generated at time iter by the forward setp, sorted according to their Hilbert index

		for(k=0;k<dx;k++)
		{
			for(i=0; i<N; i++)
			{
				x[dx*i+k]=storeX[n*iter+dx*i+k];
			}
		}

		//Sample  particles  at time iter

		for(j=0; j<Nb; j++)
		{	
			//Particle \tilde{x}_{iter+1}^j

			for(k=0;k<dx;k++)
			{
				xhh[k]=xh[dx*j+k];
			}

			//Compute backward weights at time iter	

			(*transition)(xhh, x, dx, iter+1, N, theta, wei);

			for(i=0;i<N;i++)
			{	
				wei[i]+=storeW[N*iter+i];
			}

			cc=weight(wei,W,N); 

			//Sample particle j

			ResampleBack(sim[T*j+T-1-iter], x, dx, N,  W, j, xh);   

			//Compute smoothing expectation

			for(k=0;k<dx;k++)
			{
				expx[dx*iter+k]+=xh[dx*j+k]/((double) Nb);
			}

		}
	}

	free(xhh);
	xhh=NULL;
	free(wei);
	wei=NULL;
	free(sim);
	sim=NULL;
}





































































#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/hilbert_sort.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/spatial_sort.h>
#include <CGAL/Spatial_sort_traits_adapter_d.h>
#include <boost/iterator/counting_iterator.hpp>
#include <CGAL/Cartesian_d.h>
#include <CGAL/Homogeneous_d.h>
#include <iostream>
#include <vector>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>  


extern "C" {
#include "../include/HilbertCode.hpp"
}

typedef CGAL::Cartesian_d<double> Kernel;
typedef Kernel::Point_d                                Point_d;

typedef 
  CGAL::Spatial_sort_traits_adapter_d<Kernel,Point_d*>  Search_traits_d;






void Hilbert_Sort(double *x, int *dx, int *N, double *J)
{
	int i,count;
	std::vector<Point_d> points; 

	count=0;
	for(i=0;i<*N;i++)
	{
		points.push_back(Point_d(*dx, x+count, x+count+*dx));
		count+=*dx;
	}

	std::vector<std::ptrdiff_t> indices;
  	indices.reserve(*N);
  
  	std::copy(boost::counting_iterator<std::ptrdiff_t>(0),
            boost::counting_iterator<std::ptrdiff_t>(*N),
            std::back_inserter(indices));
  
  	CGAL::hilbert_sort( indices.begin(),indices.end(),Search_traits_d(&(points[0])) );
  
  	for (i=0;i<*N;i++)
  	{
		 J[i]=indices[i];
  	}
}




















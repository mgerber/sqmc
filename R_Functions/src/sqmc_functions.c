

#include "../include/sqmc_functions.h"	


Scrambled *os1=NULL;

void Scrambling(Scrambled* os ) 
{
    Scrambled_Randomize(os);
}


void getPoints(Scrambled* os, int d, int n, double *seq) 
{
    Scrambled_GetPoints(os, d, n, seq);
}

void getPoints_HO(Scrambled* os, int d, int int_factor, int n, double *seq) 
{
    HOScrambled_GetPoints(os, d, int_factor, n, seq);
}


void Digital_getPoints(DigitalNetGenerator* os, int d, int n, double *seq) 
{
    DigitalNetGenerator_GetPoints(os, d, n, seq);
}

/***************************************************************************
	SCRAMBLED SOBOL' SEQUENCE
***************************************************************************/
void Sobol_random(int *N, int *dx, int *init, int* first_call, int *seed_val, double* x)
{
	if(*first_call==1)
	{	
		os1=Scrambled_Create(1, *dx, *N, *seed_val);
	}
	else
	{	
		if(*init==1)
		{	
			Scrambled_Destroy(os1);
			os1=Scrambled_Create(1, *dx, *N, *seed_val);
		}
	}
	Scrambling(os1);
	getPoints(os1, *dx, *N, x);	
}

void Free_Sobol_random()
{
		Scrambled_Destroy(os1);
		os1=NULL;
}

/***************************************************************************
	HIGHER ORDER SCRAMBLED SOBOL' SEQUENCE
***************************************************************************/

void HOSobol_random(int *N, int *dx, int *int_fact, int *init, int* first_call, int *seed_val, double* x)
{
	int dim=*dx*(*int_fact);
	if(*first_call==1)
	{	
		os1=Scrambled_Create(1, dim, *N, *seed_val);
	}
	else
	{	
		if(*init==1)
		{	
			Scrambled_Destroy(os1);
			os1=Scrambled_Create(1, dim, *N, *seed_val);
		}
	}
	Scrambling(os1);
	getPoints_HO(os1, *dx, *int_fact, *N, x);	
}


/***************************************************************************
	SOBOL' SEQUENCE
***************************************************************************/


void Sobol_deterministic(int *N, int *dx, double* x)
{
	DigitalNetGenerator *os2=DigitalNetGenerator_Create(1, *dx);

	DigitalNetGenerator_GetPoints(os2, *dx, *N, x); 

	DigitalNetGenerator_Destroy(os2);
	os2=NULL;
}


/***************************************************************************
	HILBERT RESAMPLING
***************************************************************************/

void HilbertResampler(double *sorted_points, double *x, int* dx, int* N, int* M, double *W, double *J)
{
	int i,j;
	double s;
	double *J1=(double*)malloc(sizeof(double)*(*N));

	Hilbert_Sort(x, dx, N, J1);
	
    	s=0;
	j=0;

	for(i=0; i<*N; i++)
	{
		s+=W[(int)J1[i]];

		while(sorted_points[j]<=s && j<*M)
		{
			J[j]=J1[i];
			j++;
		}
		if(j==*M)
		{
			break;
		}
	     
	}
	free(J1);
	J1=NULL;
}



/***************************************************************************
	INVERSE ROSENBLATT TRANORMATION OF GAUSSIAN DISTRIBUTION
***************************************************************************/


void rmnormQMC(double* points, int* dx, int* N, double *theta, double *x)
{
	int i, s,k;
	double var, var1,muI;	
	gsl_matrix *SIGMA=gsl_matrix_alloc(*dx,*dx);

	gsl_matrix *winv=NULL, *work1=NULL;
	gsl_vector *v1=NULL, *v2=NULL;
	gsl_permutation *p=NULL;

	
	//Sigma 

	for(k=0;k<*dx;k++)
	{
		for(s=0;s<*dx;s++)
		{
			gsl_matrix_set(SIGMA,k,s,theta[*dx*k+s]);
		}
	}
	
	var=pow(gsl_matrix_get(SIGMA,0,0),0.5);

	for(i=0;i<*N;i++)			//sample first dimension
	{
		x[*dx*i]=var*gsl_cdf_ugaussian_Pinv(points[*dx*i]);
	}

	for(k=1;k<*dx;k++)			//sample other dimensions
	{
		v1=gsl_vector_alloc(k);
		v2=gsl_vector_alloc(k);
		p=gsl_permutation_alloc(k);
		winv=gsl_matrix_alloc(k,k); 
		work1=gsl_matrix_alloc(k,k); 

		for(s=0;s<k;s++)
		{
			gsl_vector_set(v1,s,gsl_matrix_get(SIGMA,k,s));		//vector of covariance
		}

		gsl_matrix_view cov= gsl_matrix_submatrix(SIGMA, 0, 0, k,k);	//variance-covariance matrix
		gsl_matrix_memcpy(work1,&cov.matrix);

		gsl_linalg_LU_decomp(work1, p, &s);            
		gsl_linalg_LU_invert(work1, p, winv);				//inverse of the variance-covariance matrix

		gsl_blas_dsymv (CblasUpper, 1, winv, v1, 0, v2);		//Sima_{i,i}^{-1}Sigma_{i,j}

		gsl_blas_ddot (v2, v1, &var1);				        //Sigma_{j,i}Sima_{i,i}^{-1}Sigma_{i,j}
		
		var=pow(gsl_matrix_get(SIGMA,k,k)-var1,0.5);			//variance of x_k|x_{1:k-1}
			
		for(i=0;i<*N;i++)
		{
			muI=0;

			for(s=0;s<k;s++)
			{
				muI+=gsl_vector_get(v2,s)*(x[*dx*i+s]);
			}
		
			x[*dx*i+k]=var*gsl_cdf_ugaussian_Pinv(points[*dx*i+k])+muI;
		}
		
		gsl_vector_free(v1);
		v1=NULL;
		gsl_vector_free(v2);
		v2=NULL;
		gsl_matrix_free(winv);
		winv=NULL;
		gsl_matrix_free(work1);
		work1=NULL;
		gsl_permutation_free(p);
		p=NULL;
	}
	
	gsl_matrix_free(SIGMA);
	SIGMA=NULL;


}

/***************************************************************************
	INVERSE ROSENBLATT TRANORMATION OF t DISTRIBUTION
***************************************************************************/



void rmvtQMC(double* points, int* dx, int* N, double *theta, double *nu, double *x)
{
	int i, s,k,j;
	double var, var1,muI,y;	
	gsl_matrix *SIGMA=gsl_matrix_alloc(*dx,*dx);

	gsl_matrix *winv=NULL, *work1=NULL;
	gsl_vector *v1=NULL, *v2=NULL;
	gsl_permutation *p=NULL;

	
	//Sigma 

	for(k=0;k<*dx;k++)
	{
		for(s=0;s<*dx;s++)
		{
			gsl_matrix_set(SIGMA,k,s,theta[*dx*k+s]);
		}
	}
	
	var=pow(gsl_matrix_get(SIGMA,0,0),0.5);

	for(i=0;i<*N;i++)			//sample first dimension
	{
		x[*dx*i]=var*gsl_cdf_tdist_Pinv(points[*dx*i], *nu);
	}

	for(k=1;k<*dx;k++)			//sample other dimensions
	{
		v1=gsl_vector_alloc(k);
		v2=gsl_vector_alloc(k);
		p=gsl_permutation_alloc(k);
		winv=gsl_matrix_alloc(k,k); 
		work1=gsl_matrix_alloc(k,k); 

		for(s=0;s<k;s++)
		{
			gsl_vector_set(v1,s,gsl_matrix_get(SIGMA,k,s));		//vector of covariance
		}

		gsl_matrix_view cov= gsl_matrix_submatrix(SIGMA, 0, 0, k,k);	//variance-covariance matrix
		gsl_matrix_memcpy(work1,&cov.matrix);

		gsl_linalg_LU_decomp(work1, p, &s);            
		gsl_linalg_LU_invert(work1, p, winv);				//inverse of the variance-covariance matrix

		gsl_blas_dsymv (CblasUpper, 1, winv, v1, 0, v2);		//Sima_{i,i}^{-1}Sigma_{i,j}

		gsl_blas_ddot (v2, v1, &var1);				        //Sigma_{j,i}Sima_{i,i}^{-1}Sigma_{i,j}
		
			
		for(i=0;i<*N;i++)
		{
			muI=0;
			for(s=0;s<k;s++)
			{
				muI+=gsl_vector_get(v2,s)*x[*dx*i+s];
			}

			var=*nu;
			for(j=0;j<k;j++)
			{
				y=0;
				for(s=0;s<k;s++)
				{
					y+=x[*dx*i+s]*gsl_matrix_get(winv,s,j);
				}
                        	y*=x[*dx*i+j];
				var+=y;
			}
			var*=(gsl_matrix_get(SIGMA,k,k)-var1)/(double)(*nu+k);

			x[*dx*i+k]=pow(var,0.5)*gsl_cdf_tdist_Pinv(points[*dx*i+k], (double)(*nu+k))+muI;
		}
		
		gsl_vector_free(v1);
		v1=NULL;
		gsl_vector_free(v2);
		v2=NULL;
		gsl_matrix_free(winv);
		winv=NULL;
		gsl_matrix_free(work1);
		work1=NULL;
		gsl_permutation_free(p);
		p=NULL;
	}
	
	gsl_matrix_free(SIGMA);
	SIGMA=NULL;


}



















	

##################################################
# HILBERT SORT
###################################################

#Input:  
#x:       a (N\times d) matrix, each row represents a point in R^d

#Output: 
		#A permutation of {1,...,N}


Hilbert_Sort_Index<-function(x){
	xx<-as.matrix(x)
	return(1+.C('Hilbert_Sort', as.double(c(t(x))), as.integer(ncol(xx)), as.integer(nrow(xx)), res=double(nrow(xx)))$res)
}



##################################################
# HILBERT RESAMPLER
###################################################

#Inputs:  
#points:   A point set of M points in (0,1), sorted by ascending order
#x:        a (N\times d) matrix, each row represents a point in R^d
#weights:  a vector of N normalized weights.

#Output: 
		#A set of M integers in {1,...,N}




Hilbert_Resampling<-function(points, x, weights){
	xx<-as.matrix(x)
	return(1+.C('HilbertResampler', as.double(points), as.double(c(t(x))), as.integer(ncol(xx)), as.integer(nrow(xx)),
		 as.integer(length(points)),  as.double(weights), res=double(length(points)))$res)
}
		
		


##################################################
# (R)QMC GENERATOR
###################################################


#Inputs: 
#N:	        number of points in each sample
#d:          dimension of the points in the samples
#scrambling: If TRUE, Owen's scrambling is applied,
#		   If FALSE, deterministic Sobol' sequence

#Output: 
#        A N \times d matrix of points in (0,1)^{d}
#	    The points are sorted according to their first
#	    components


if(exists('Init_Sobol')){
	if(First_call==0){
		 .C('Free_Sobol_random')	 
	}
}

Control_Sobol_dim<<-0
Control_Sobol_N<<-0
Init_Sobol<<-1
First_call<<-1


Sobol<-function(N,d, scrambling=TRUE, HO=FALSE, seed=-1, interlacing_factor=1){
	if(HO || scrambling)
	{
		if(First_call==1){
			Control_Sobol_dim<<-d
			Control_Sobol_N<<-N
			Control_Sobol_factor<<-interlacing_factor
	     }else{
			if(Control_Sobol_dim==d && Control_Sobol_N==N && Control_Sobol_factor==interlacing_factor){
				Init_Sobol<<-0
			}else{
		 		.C('Free_Sobol_random')
				Init_Sobol<<-1
				Control_Sobol_dim<<-d
				Control_Sobol_N<<-N
				Control_Sobol_factor<<-interlacing_factor
			}
		}
		if(HO && interlacing_factor>1)
		{
			points<-matrix(.C('HOSobol_random', as.integer(N), as.integer(d), as.integer(interlacing_factor),
				as.integer(Init_Sobol), as.integer(First_call), as.integer(seed), res=double(d*N))$res,N,d,byrow=TRUE)
			J<-order(points[,1])
			points<-points[J,]
		}else{
			points<-matrix(.C('Sobol_random', as.integer(N), as.integer(d), as.integer(Init_Sobol),
          	         as.integer(First_call), as.integer(seed), res=double(d*N))$res, N, d, byrow=TRUE)
		}
		First_call<<-0
		Init_Sobol<<-0
	}else{
		points<-matrix(.C('Sobol_deterministic', as.integer(N), as.integer(d), res=double(d*N))$res,N, d, byrow=TRUE)
	}
	return(points)	 	
}



free_Sobol<-function(){
	if(Init_Sobol==0){
	   .C('Free_Sobol_random')	
	    Control_Sobol_dim<<-0
	    Control_Sobol_N<<-0
         Init_Sobol<<-1
         First_call<<-1
	}
	return("Memory released")
}

#####################################################################
# GENERATE GAUSSIAN VARIATES USING INVERSE ROSENBLATT TRANSFORMATION
#####################################################################


#Inputs:
 
# points:   A (N \times d) matrix of points in (0,1)

# d:        dimension of the points in the samples

# mu:       either a vector of length ‘d’, representing the mean value,
#           or  a matrix whose rows represent
#           different mean vectors

#Sigma:     a symmetric positive-definite matrix representing the
#           variance-covariance matrix of the distribution

#Output:    (N \times d) matrix of points in R^{d}



rmnorm_qmc<-function(points, d, mu, Sigma){
	N<-nrow(as.matrix(points))
	
	if(is.vector(mu)){
	    mu_bis=matrix(rep(mu,N),N,d, byrow=TRUE)
	}else{
	    mu_bis<-mu
	}

	result<-.C('rmnormQMC', as.double(c(t(points))), as.integer(d), as.integer(N), as.double(c(Sigma)), res=double(N*d))$res
	return(mu_bis+ matrix(result,N, d, byrow=TRUE))
}

#####################################################################
# GENERATE t VARIATES USING INVERSE ROSENBLATT TRANSFORMATION
#####################################################################

#Inputs:
 
# points:   A (N \times d) matrix of points in (0,1)

# d:        dimension of the points in the samples

# mu:       either a vector of length ‘d’, representing the mean value,
#           or  a matrix whose rows represent
#           different mean vectors

#nu         degree of freedom

#Sigma:     a symmetric positive-definite matrix representing the
#           variance-covariance matrix of the distribution

#Output:    (N \times d) matrix of points in R^{d}

rmvt_qmc<-function(points, d, mu, Sigma, nu){
	N<-nrow(as.matrix(points))
	
	if(is.vector(mu)){
	    mu_bis=matrix(rep(mu,N),N,d, byrow=TRUE)
	}else{
	    mu_bis<-mu
	}

	result<-.C('rmvtQMC', as.double(c(t(points))), as.integer(d), as.integer(N), as.double(c(Sigma)), as.double(nu), res=double(N*d))$res
	return(mu_bis+ matrix(result,N, d, byrow=TRUE))
}
















	 





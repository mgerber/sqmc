


#include<stdlib.h>  		
#include<stdio.h>	
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>

#include "Generate_RQMC.hpp"	
#include "HilbertCode.hpp"

typedef void *DigitalNetGenerator;

typedef void *Scrambled;

void Scrambling(Scrambled*);

void getPoints(Scrambled*, int, int, double*);

void getPoints_HO(Scrambled*, int, int, int, double*);
 
Scrambled * Scrambled_Create(int,int, int, int);

void Scrambled_Destroy(Scrambled*);

void Scrambled_Randomize(Scrambled*);

void Scrambled_GetPoints(Scrambled*, int, int, double*);

void HOScrambled_GetPoints(Scrambled*, int, int, int, double*);



DigitalNetGenerator *DigitalNetGenerator_Create(int, int);

void DigitalNetGenerator_Destroy(DigitalNetGenerator*); 

void DigitalNetGenerator_GetPoints(DigitalNetGenerator*, int, int, double*); 

void Digital_getPoints(DigitalNetGenerator*, int, int, double*); 


void Sobol_random(int*, int*, int*, int*,int*, double*);

void HOSobol_random(int*, int*, int*, int*, int*,int*, double*);

void Free_Sobol_random();

void Sobol_deterministic(int*, int*, double*);

void HilbertResampler(double*, double*, int*, int*, int*, double*, double*);

void rmnormQMC(double*, int*, int*, double*, double *x);


void rmvtQMC(double*, int*, int*, double*, double*, double*);






















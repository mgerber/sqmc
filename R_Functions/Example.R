

dyn.load('lib/sqmc.so')	
source('src/sqmc.R')

###HILBERT SORT#############

N<-10					#number of points
d<-4					#dimension of each points				
x<-matrix(rnorm(N*d,2,2),N,d)		#(N\times d) matrix

Hilbert_Sort_Index(x)			#Output: A permutation of {1,...,N}

### (R)QMC GENERATOR#######

N<-2^12					#Number of points in each sample
d <-2					#dimension of the point sets


points<-Sobol(N,d , scrambling=TRUE)	#points  is a (d times N) matrix
					#The points are sorted according
					#to their first component

									
plot(points[,1],points[,2]) 		#plot first two dimensions


points<-Sobol(N,d , scrambling=TRUE,   	#higher order scrambled Sobol'
	  HO=TRUE, interlacing_factor=2)


plot(points[,1],points[,2]) 	        #plot first two dimensions

free_Sobol()				#Release memory. Nothing bad
					#will happen if you don't do
					#this.
#### HILBERT RESAMPLING

library('gtools')			#for the Dirichelet distribution
					#(used to generate the weights in the example below)


N_1<-100				#number of points to sample
d<-4					#dimension of each points			
x<-matrix(rnorm(N_1*d,2,2),N_1,d)	#(N\times d) matrix
Weights<-c(rdirichlet(1, rep(1,N_1)))	#generate random normalized weights

N_2<-10					#Number of points to sample				
points<-Sobol(N_2,1 , scrambling=TRUE)	#sorted points set in (0,1)
	

Hilbert_Resampling(points[,1], x, Weights)	#A set of N_2 integers in {1,...,N_1}

#### GENERATE QMC GAUSSIAN VARIATES USING
#### INVERSE ROSENBLATT TRANSFORMATION


N<-10000				#number of points
d<-4					#dimension of each points			
points<-Sobol(N, d , scrambling=TRUE)	#point set in (0,1)^d


mu<-rep(2,d)				#vector of means of size d
									

Sigma<-diag(1,d)			#(d\times d) covariance matrix
									

res<-rmnorm_qmc(points, d, mu, Sigma)	#(N\times d) matrix in R^d.
					#if uniform random numbers are
					#used as input, then for k\in 1:N
					#res[k,]\sim N(mu,Sigma)

hist(res[,1])


#### GENERATE QMC  t VARIATES USING
#### INVERSE ROSENBLATT TRANSFORMATION 


N<-100000				#number of points
d<-4					#dimension of each points	
nu<-10					#degrees of freedom		
points<-Sobol(N, d , scrambling=TRUE)	#points set in (0,1)^d


mu<-rep(2,d)				#(N\times d) matrix of locations

Sigma<-diag(1,d)	                #(d\times d) scale matrix
								    								

res<-rmvt_qmc(points, d, mu, Sigma,nu)  #(N\times d) matrix in R^d.
					#if uniform random numbers are
					#used as input, then for k\in 1
					#res[k,]\sim t_nu(mu[k,],Sigma)





























